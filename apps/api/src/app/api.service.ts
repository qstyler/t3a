import { Injectable } from '@nestjs/common';
import { DbService } from '@t3a/db';
import { Record } from '@t3a/domain';

@Injectable()
export class ApiService {

  constructor(
    private dbService: DbService,
  ) {
  }

  async getRecords(): Promise<Record[]> {
    return this.dbService.getRecords();
  }
}
