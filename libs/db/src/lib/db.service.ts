import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Record } from '@t3a/domain';
import { RecordSchema, RecordDocument } from './schemas';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class DbService {

  constructor(
    @InjectModel(RecordSchema.name) private recordModel: Model<RecordDocument>,
  ) {
  }

  async getRecords(): Promise<Record[]> {
    return this.recordModel.find({}, {
      '__v': 0,
      '_id': 0,
    });
  }

  addRecord(record: RecordSchema) {
    return this.recordModel.create(record);
  }

  removeRecords() {
    return this.recordModel.deleteMany();
  }

  insertMany(items: Record[]) {
    return this.recordModel.insertMany(items, { ordered: false, rawResult: true });
  }

  async filterInvalid(items: Record[]) {
    return (await Promise.all(items.map(async item => {
      try {
        await this.recordModel.validate(item);
      } catch (e) {
        return null;
      }

      return item;
    }))).filter(Boolean);
  }
}
