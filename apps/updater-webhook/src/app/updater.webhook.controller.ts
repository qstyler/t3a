import { Controller, Get } from '@nestjs/common';

import { UpdaterWebhookService } from './updater.webhook.service';

@Controller('')
export class UpdaterWebhookController {
  constructor(private readonly appService: UpdaterWebhookService) {}

  @Get('recordsUpdated')
  recordsUpdated() {
    return this.appService.recordsUpdated();
  }
}
