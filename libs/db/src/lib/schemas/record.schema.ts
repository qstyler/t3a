import { Prop, Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Record as RecordInterface } from '@t3a/domain';

export type RecordDocument = RecordSchema & Document;

@Schema()
export class RecordSchema implements RecordInterface {
  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  image: string;

  @Prop()
  description: string;
}
