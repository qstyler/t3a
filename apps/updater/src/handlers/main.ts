import { ContextIdFactory, NestFactory } from '@nestjs/core';
import {
  Context,
  SNSEvent,
  APIGatewayProxyResult,
} from 'aws-lambda';
import { UpdaterModule } from '../app/updater.module';
import { INestApplicationContext } from '@nestjs/common';
import { UpdaterService } from '../app/updater.service';

let app: INestApplicationContext;

async function bootstrap(): Promise<INestApplicationContext> {
  const app = await NestFactory.create(UpdaterModule);
  await app.init();

  return app;
}

export const handler = async (
  event: SNSEvent,
  context: Context,
): Promise<APIGatewayProxyResult>  => {

  const instance = await bootstrap();

  const contextId = ContextIdFactory.create();
  instance.registerRequestByContextId({ context }, contextId);
  const service = await instance.resolve<UpdaterService>(UpdaterService, contextId);

  const result = await service.updateRecords();

  return { statusCode: 200, body: JSON.stringify(result) };
};
