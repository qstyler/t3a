import { Module } from '@nestjs/common';
import { DbService } from './db.service';
import { MongooseModule, SchemaFactory } from '@nestjs/mongoose';
import { RecordSchema } from './schemas';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/records'),
    MongooseModule.forFeature([
      { name: RecordSchema.name, schema: SchemaFactory.createForClass(RecordSchema) }
    ]),
  ],
  providers: [DbService],
  exports: [DbService],
})
export class DbModule {
}
