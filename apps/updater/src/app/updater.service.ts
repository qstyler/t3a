import { HttpService, Injectable } from '@nestjs/common';
import { DbService } from '@t3a/db';
import { CsvParser } from 'nest-csv-parser';
import { Readable } from 'stream';
import { RecordSchema } from '@t3a/db';

const url = 'https://docs.google.com/spreadsheet/ccc?key=0Aqg9JQbnOwBwdEZFN2JKeldGZGFzUWVrNDBsczZxLUE&single=true&gid=0&output=csv';

@Injectable()
export class UpdaterService {
  constructor(
    private dbService: DbService,
    private httpService: HttpService,
    private readonly csvParser: CsvParser,
  ) {
  }

  async updateRecords() {

    const { data } = await this.httpService.get(url)
      .toPromise();

    const stream = Readable.from(data);

    let items;
    try {
      ({ list: items } = await this.csvParser.parse(stream, RecordSchema, undefined, undefined, {
        strict: false,
        separator: ',',
      }));
    } catch (e) {
      console.error(`error parsing csv file: ${e.message}`);
      return;
      // alternatively throw Error and/or publish “update failed” topic
    }

    const valid = await this.dbService.filterInvalid(items);

    if (!valid.length) {
      console.log('csv file contains no valid records');
      return;
      // alternatively throw Error and/or publish “update failed” topic
    }

    await this.dbService.removeRecords();

    return this.dbService.insertMany(valid);
  }
}
