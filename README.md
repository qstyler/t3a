# t3a: touch tribe test assignment

## Description

The project “monitors” a CSV file for changes, updates database when changed and is able to serve saved records back on request.

## Architecture

![t3s architecture](./architecture.png)

* ApiGateway — front-facing service to route requests between API and Updater Webhook.
* API — responsible for querying the database for saved records
* Updater — downloads and processes CSV, then updates the records in the database
* Updater Webhook — publishes “recordsUpdated” topic in the SNS
* SNS — Simple Notification Service for pub/sub architecture
* MongoDB — persistent storage

## Reactive manifesto:

* **Responsive**: system is running independent lambdas that will always respond to user back with data.
* **Resilient**: a copy of the latest csv file is always stored in the database. If “updater” fails to download or parse the CSV file it will not proceed to the next steps like saving 
* **Elastic**: all services used in the architecture are automatically scaled based on the load. Additionally, beanstalk can be added.
* **Message Driven**: It is obviously built with pub/sub pattern which is a message driven architecture.

## Technical Details

The project is built with [nx](https://nx.dev/) monorepo management tool and [NestJS](https://nestjs.com/) framework.

It uses [serverless](https://www.serverless.com/) framework for building and managing lambdas and [serverless-offline](https://www.serverless.com/plugins/serverless-offline) plugin to run it offline.

Project depends on [MongoDB](https://www.mongodb.com/) database for records storage and [SNS](https://aws.amazon.com/sns/) (Amazon's simple notification service) for communication between lambdas.

## Requirements

Requires [docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/) for running containerized instance of mongo.

Run this command when ready:

```shell
docker-compose -f docker/docker-compose.yml -p "t3a" up --detach
```

Or just start mongo directly, we don't have a lot of dependencies
```shell
docker run -d -p 27017:27017 --name "mongo" mongo:latest
```

## Installation

Installation is very simple. First just run:
```shell
npm i
```

## Running

First build all projects:
```shell
nx run api:build
nx run updater:build
nx run updater-webhook:build
```

Then start them:
```shell
npm run start:api
npm run start:updater
npm run start:updater-webhook
```

Starting `updater` service will also activate `serverless-offline-sns` plugin that will start a local instance of SNS that is available on port `4002`.

Let's try sending some requests when everything is running
```shell
👉 t3a git:(master) curl --location --request GET 'http://localhost:3333/getRecords'
[]
```

Trigger the webhook!
```shell
👉 t3a git:(master) curl --location --request GET 'http://localhost:3335/recordsUpdated'
{"ResponseMetadata":{"RequestId":"482a4788-013d-429c-88a6-45f06d47853b"},"MessageId":"dda26695-4c86-42c2-a135-445f5fd8fb85"}
```

You see some technical SNS data in a response. Not sure if we need to return anything, but `204 No Content`.

But let's check if there are any new records in our database. (with a little bit of help of `jj` for json formatting)

```shell
👉 t3a git:(master) curl --location --request GET 'http://localhost:3333/getRecords' | jj -p
```

![response](./response.png)


## Development

To run and watch, just do:
```shell
npm run start:api --watch
```

## Filtering algorithm notes

Error handling choices were ones of the most uncertain things in the task. So I made my own decisions:

* No update happens if file is empty or link is broken

Technically, CSV file is always valid (it can just contain crooked data). So in my scenario if there is at least one valid record, it will be used.

Image is required, therefore if the whole CSV file is semantically correct but contains no images, the update won't happen.

All these edge cases should be discussed with the client for the right choice.

## Bonus
Make use of nginx with `proxy_pass` emulate Api Gateway.

`serverless.yml` file describes not just a single function, but a whole application that may be combined of different lambdas (but only one in our case). That's why we run all our applications separately. And of course we cannot bind them to the same port.

If we had a frontend application it would be much easier to talk to one endpoint (localhost:3000).


That's what we use nginx for. It listens to the port 3000 and proxies all requests to the corresponding lambdas.

This way:

```nginx configuration
  server {
    server_name updater-webhook;

    location /recordsUpdated {
      proxy_pass http://host.docker.internal:3334;
    }
  }
```

## Needs to be done:

### Configuration
For environments management a proper configuration module has to be added. So far I just hard-coded a mongo url and some other constants (that should be env variables).

### Proper file-change monitoring
At this there is only one way to trigger the update procedure: to activate a webhook.

It needs to be discussed with the client, how crucial the recent data is. There are many other ways for monitoring if the data changes.

Possible solutions:
* **Easy mode:** create a lambda that can be trigger on a schedule to check for changes. Probably based on hash sum of the file.
* **Heavy mode:** do this upon every request (NOT RECOMMENDED) 

Anyway. This is easily integrated into the system because needs only one line of code to publish a message. 

## Failed to do
[nx-serverless](https://www.npmjs.com/package/@ns3/nx-serverless) + [serverless-offline-sns](https://www.npmjs.com/package/serverless-offline-sns) + nest + nx@13 + webpack@5 

`nx-serverless` is a great nx plugin that enables lifecycle management of aws lambdas in nx monorepo. It is very easy to create and run applications written even in TypeScript. This even includes auto-reload when in watch mode.

However, it doesn't make a very god friends with nest. When you try to create your lambda as a NestJS applications in nx@13 that uses webpack5, you are going to get lots of type errors. If you downgrade to nx@12 and webpack@4 you will still be getting errors, but the offline version of lambda will start anyway.

But this will only work with http event for the function trigger. When you try to make use of `serverless-offline-sns` to trigger your function based on SNS topic, the plugin will fail to find a handler and won't work.

I gonna need a webpack-nerd to help me with the configuration of this. 

## Improvements
### Splitting updater's logic

At this moment “updater” is responsible for many purposes, such as:

* Getting a csv file (http download in our case)
* Parsing it
* Validating
* Storing filtered records to the database

All these operations could be split into separate lambdas with their own responsibilities. And communication between them can be implemented with [AWS SQS](https://aws.amazon.com/sqs/).  

### Seamless update
In the code we remove all records before inserting the new ones. This might result that there would be a few milliseconds when the data would not be available.

To avoid this a “blue-green” update can be implemented.

A records model might have one extra boolean field. Let's call it “active”. Only active records are served.

During the import we do not kill all previous, all new records are created not active.

Then we update the whole collection with a query like this:

```javascript
{ $set: { active: { $not: "$active" } } }
```
And only then delete old not active records.

This might be a little easier with transactions in relational db.
