import { Test, TestingModule } from '@nestjs/testing';
import { DbModule, DbService } from '@t3a/db';
import { getModelToken } from '@nestjs/mongoose';

describe('Db Service suite', () => {
  let service: DbService;
  const validate = jest.fn();

  let app: TestingModule;

  beforeAll(async () => {
    app = await Test
      .createTestingModule({
        providers: [
          DbService,
          {
            provide: getModelToken('RecordSchema'),
            useValue: {
              validate,
            },
          },
        ],
      })
      .compile()
    ;

    service = app.get<DbService>(DbService);
  });

  afterAll(() => {
    app.close();
  });

  afterEach(() => {
    validate.mockReset();
  });

  const recordMock = {
    title: '1',
    image: '1',
    description: '1',
  };

  it('should validate a full valid object', async () => {
    const actual = await service.filterInvalid([recordMock]);

    expect(actual).toHaveLength(1);
  });

  it('should invalidate if exception is thrown', async () => {
    validate.mockRejectedValue(new Error());

    const actual = await service.filterInvalid([recordMock]);

    expect(actual).toHaveLength(0);
  });

  it('should filter one invalid', async () => {
    validate
      .mockRejectedValueOnce(new Error())
      .mockReturnValueOnce(true)
    ;

    const actual = await service.filterInvalid([recordMock, recordMock]);

    expect(actual).toHaveLength(1);
  });

});
