import { Module } from '@nestjs/common';

import { UpdaterWebhookController } from './updater.webhook.controller';
import { UpdaterWebhookService } from './updater.webhook.service';
import { SnsModule } from '@t3a/sns';

@Module({
  imports: [SnsModule],
  controllers: [UpdaterWebhookController],
  providers: [UpdaterWebhookService],
})
export class UpdaterWebhookModule {}
