import { Injectable } from '@nestjs/common';
import { SNS } from 'aws-sdk';

@Injectable()
export class SnsService {
  private readonly sns: SNS;

  constructor() {
    this.sns = new SNS({
      endpoint: 'http://127.0.0.1:4002',
      region: 'eu-central-1'
    })
  }

  async publish() {
    return await this.sns.publish({
      Message: 'Records Updated',
      TopicArn: 'arn:aws:sns:eu-central-1:123456789000:recordsUpdated'
    }).promise();
  }

}
