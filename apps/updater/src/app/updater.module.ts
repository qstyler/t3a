import { HttpModule, Module } from '@nestjs/common';

import { UpdaterService } from './updater.service';
import { DbModule } from '@t3a/db';
import { CsvModule } from 'nest-csv-parser';

@Module({
  imports: [DbModule, HttpModule, CsvModule],
  providers: [UpdaterService],
})
export class UpdaterModule {}
