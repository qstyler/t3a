import { Test, TestingModule } from '@nestjs/testing';
import { UpdaterService } from './updater.service';
import { DbModule, DbService } from '@t3a/db';
import { HttpModule, HttpService } from '@nestjs/common';

import { CsvModule } from 'nest-csv-parser';
import fs from 'fs';

describe('Updater Service Suite', () => {

  let service: UpdaterService;
  const insertMany = jest.fn();
  const removeRecords = jest.fn();
  const toPromise = jest.fn();
  const filterInvalid = jest.fn();

  let app: TestingModule;

  beforeAll(async () => {

    app = await Test
      .createTestingModule({
        providers: [UpdaterService],
        imports: [DbModule, HttpModule, CsvModule],
      })
      .overrideProvider(HttpService)
      .useValue({
        get: () => ({
          toPromise,
        }),
      })
      .overrideProvider(DbService)
      .useValue({
        removeRecords,
        insertMany,
        filterInvalid,
      })
      .compile();


    service = app.get<UpdaterService>(UpdaterService);
  });

  afterAll(() => {
    app.close();
  })

  afterEach(() => {
    insertMany.mockReset();
    removeRecords.mockReset();
    toPromise.mockReset();
    filterInvalid.mockReset();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('update records', async () => {

    const filteredArray = Array(24);

    // this mocking is not really needed because we mock everything else
    // but it is useful to step through every line of function in a debug mode
    const testApplicationData = fs
      .readFileSync(__dirname + '/__mocks__/test_application_data.csv')
      .toString();
    toPromise.mockReturnValue(Promise.resolve({ data: testApplicationData }));

    filterInvalid.mockReturnValue(filteredArray);

    await service.updateRecords();
    expect(removeRecords).toHaveBeenCalledTimes(1);
    expect(insertMany).toHaveBeenCalledWith(
      filteredArray,
    );
  });

  it('should ignore malformed or empty file', async () => {

    toPromise.mockReturnValue(Promise.resolve({ data: "" }))
    filterInvalid.mockReturnValue([]);

    await service.updateRecords();
    expect(removeRecords).not.toBeCalled();

  });

});
