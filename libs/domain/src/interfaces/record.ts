export interface Record {
  title: string;
  description?: string;
  image: string;
}
