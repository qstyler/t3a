import { Injectable } from '@nestjs/common';
import { DbService } from '@t3a/db';
import { Record } from '@t3a/domain';
import { SnsService } from '@t3a/sns';

@Injectable()
export class UpdaterWebhookService {

  constructor(
    private snsService: SnsService,
  ) {
  }

  async recordsUpdated() {
    return this.snsService.publish();
  }
}
