import { Module } from '@nestjs/common';

import { ApiController } from './api.controller';
import { ApiService } from './api.service';
import { DbModule } from '@t3a/db';

@Module({
  imports: [DbModule],
  controllers: [ApiController],
  providers: [ApiService],
})
export class ApiModule {}
